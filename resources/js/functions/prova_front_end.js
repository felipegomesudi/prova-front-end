import {isValidEmail} from "../functions/validate"
import { _alert, _confirm } from "../functions/message";
import { getFromStorage, setInStorage, checkStorage, clearStorage } from "../functions/storage";

$(document).ready(function(){
    $("#Name_session").text(checkStorage("Name_value") ? getFromStorage("Name_value") : "");
    $("#Email_session").text(checkStorage("Email_value") ? getFromStorage("Email_value") : "");
    $("#Phone_session").text(checkStorage("Phone_value") ? getFromStorage("Phone_value") : "");
    $("#Subject_session").text(checkStorage("Subject_value") ? getFromStorage("Subject_value") : "");
    $("#Message_session").text(checkStorage("Message_value") ? getFromStorage("Message_value") : "");
});

$(document).on("keyup", "#formFrontEnd input[name*='Name']", function(e){
    $("#Name_value").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd input[name*='Email']", function(e){
    $("#Email_value").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd input[name*='Phone']", function(e){
    $("#Phone_value").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd input[name*='Subject']", function(e){
    $("#Subject_value").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd textarea[name*='Message']", function(e){
    $("#Message_value").text($(this).val());
});

$(document).on("click", "#submitFrontEnd", function(e){

    var count = 0,
        form = $("#formFrontEnd");

    $("textarea:not(.g-recaptcha-response), input:visible:not([type='checkbox']):not(.search)", form).each(function() {
        if($(this).val().trim() === "")
            count++;

        if($(this).val().trim() === "")
            console.log($(this))
    });

    if(count === 0) {

        var _email = $("#formFrontEnd input[name*='Email']").val();
        if(!isValidEmail(_email)){
            _alert("", "Informe um e-mail válido.", "warning");
            return false;
        }

        var _phone = $("#formFrontEnd input[name*='Phone']").val();
        if(!isValidPhone(_phone)){
            _alert("", "Informe um telefone válido.", "warning");
            return false;
        }

        sendFrontEnd();

    } else {
        _alert("Ops...", "Preencha todos os campos corretamente", "error");
    }
    
    return false;
})

function isValidPhone(phoneToValidate){

    var phone = new RegExp(/(^|\()?\s*(\d{2})\s*(\s|\))*(9?\d{4})(\s|-)?(\d{4})($|\n)/u);

    if (phone.test(phoneToValidate))
        return true
    else
        return false
}

function sendFrontEnd() {

    var form = $("#formFrontEnd").serializeArray();
    var form_data = {};
    $.each(form, function(i, v) {
        form_data[v.name] = v.value;
    });

    setInStorage("Name_value", form_data["Name"]);
    setInStorage("Email_value", form_data["Email"]);
    setInStorage("Phone_value", form_data["Phone"]);
    setInStorage("Subject_value", form_data["Subject"]);
    setInStorage("Message_value", form_data["Message"]);

    _confirm({
        title: "",
        text: "Dados enviados com sucesso.",
        type: "success",
        confirm: {
            text: "OK"
        },
        cancel: {
            text: "Cancelar"
        },
        callback: function () {
            location.reload();
        }
    }, false);

}

$(document).on("click", "#clearSessionProva", function(e){

    clearStorage("Name_value");
    clearStorage("Email_value");
    clearStorage("Phone_value");
    clearStorage("Subject_value");
    clearStorage("Message_value");

    _confirm({
        title: "",
        text: "Dados de sessão limpos com sucesso.",
        type: "success",
        confirm: {
            text: "OK"
        },
        cancel: {
            text: "Cancelar"
        },
        callback: function () {
            location.reload();
        }
    }, false);

});